# Clone the repository
	cd ~/git/bitbucket
	git clone git@bitbucket.org:sugizo/sugizo.bitbucket.io.git
	
# Create File
	cd ~/git/bitbucket/sugizo.bitbucket.io
	
# Add, commit, and push your changes
	git add --all
	git commit -m "Initial commit"
	git push -u origin master

# Add, commit, and push your changes with tag
	git add --all
	git commit -m "v0.0"
	git tag v0.0
	git tag
	git push -u origin master
	git push origin --tags
